# Beeps to bits
CTFs like to have `.wav` files in the following form:

![](md_img/2021-05-13-01-02-07.png)

The "beeps"/pulses correspond to individual low or high bits respectively. The above screenshot's 32 second time span would decode to following 4 bytes:
```
01010101 01110111 01110011 00111000
```

This script decodes such a sequence.

## Configuration
Every challenge is a bit different. This script thus cannot be a universal solver.

Check the constants at the top to configure things.