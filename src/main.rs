#![feature(iter_advance_by)]

use std::fs::File;
use std::path::Path;
use wav::BitDepth;

const FILE_NAME: &str = "TODO.wav";
const ENCODED_BIT_PER_SECOND: f32 = 1.0;
const LOW_SIGNAL_CHAR: &str = "0";
const HIGH_SIGNAL_CHAR: &str = "1";
// sometimes a pause is introduced after e.g. 8 pulses to emphasize to possible conversion to bytes
const SKIP_EVERY_NTH_PULSE: usize = 0;

fn pulse_diff_from_seen(pulse: u32, seen: u32) -> bool {
    let abs_diff = (seen as f32 - pulse as f32).abs();
    abs_diff > 0.1 * pulse as f32
}

fn decode(mut data: impl Iterator<Item = u32>, step: usize) -> String {
    // pulses start flat, grow, and then shrink again. Step 1 unit to ensure we're not exactly on the flat anymore
    data.next();
    // `tmp_seen` is a temp cache used to store seen pulses before the low/high values are known. It's also what is used to compare new pulses against to get to know low/high values
    let mut tmp_seen = vec![];
    let mut halfway = None;
    data.step_by(step).enumerate().fold(String::new(), |mut result, (ith_pulse, pulse)|{
        // Rust sees the `modulo 0` but not the preceding `!= 0` check
        #[allow(unconditional_panic)]
        if SKIP_EVERY_NTH_PULSE != 0 && ith_pulse % SKIP_EVERY_NTH_PULSE == 0 {
            return result;
        }

        #[allow(clippy::collapsible_else_if)]
        // if we don't know what values determine whether a pulse is high or low
        if halfway.is_none() {
            // try determining by comparing this and previous pulses
            if tmp_seen.iter().any(|seen| pulse_diff_from_seen(pulse, *seen)) {
                halfway = Some((tmp_seen[0] + pulse) / 2);

                // consume cache
                for seen in tmp_seen.iter() {
                    if *seen <= halfway.unwrap() {
                        result += LOW_SIGNAL_CHAR;
                    } else {
                        result += HIGH_SIGNAL_CHAR;
                    }
                }

                // add pulse itself
                if pulse <= halfway.unwrap() {
                    result += LOW_SIGNAL_CHAR;
                } else {
                    result += HIGH_SIGNAL_CHAR;
                }
            }
            // if no decision can be made yet, add current pulse to cache and look at next
            else {
                tmp_seen.push(pulse);
            }
        }
        // if we do know how to determine high/low pulses, then just do that
        else {
            if pulse <= halfway.unwrap() {
                result += LOW_SIGNAL_CHAR;
            } else {
                result += HIGH_SIGNAL_CHAR;
            }
        }

        result
    })
}

fn main()
{
    let mut inp_file = File::open(Path::new(FILE_NAME)).unwrap();
    let (header, data) = wav::read(&mut inp_file).unwrap();

    let step = (header.bytes_per_second as f32 / header.bytes_per_sample as f32 / ENCODED_BIT_PER_SECOND) as usize;
    println!("Amount bytes stepped per encoded bit: {}", step);

    match data {
        BitDepth::Eight(data) => {
            println!("{}", decode(data.into_iter().map(|i| i as u32), step));
        },
        BitDepth::Sixteen(data)=>{
            println!("{}", decode(data.into_iter().map(|i| i as u32), step));
        },
        BitDepth::TwentyFour(data) => {
            println!("{}", decode(data.into_iter().map(|i| i as u32), step));
        }
        BitDepth::ThirtyTwoFloat(data) => {
            println!("{}", decode(data.into_iter().map(|i| i as u32), step));
        }
        _ => panic!("No data or unknown bit depth")
    }
}
